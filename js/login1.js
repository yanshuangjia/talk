const loginIdVallidator = new FieldValidator('txtLoginId',async function(val){
    if(!val){
        return '请填写账号';
    }
});

const loginPwdVallidator = new FieldValidator('txtLoginPwd',async function(val){
    if(!val){
        return '请输入密码';
    }
});

const form = $('.user-form');
form.onsubmit =async function(e){
    //阻止事件的默认行为，让表单不刷新
    e.preventDefault();
    const result = await FieldValidator.validate(
        loginIdVallidator,
        loginPwdVallidator,
    );
    if(!result){
        return;
    }
    const resp = await API.login(
        {
            loginId:loginIdVallidator.input.value,
            loginPwd:loginPwdVallidator.input.value,
        }
    )
    if(resp.code === 0){
        alert('登陆成功,跳转到首页');
        location.href = './index.html';
    }else{
        loginPwdVallidator.p.innerText = '账号或密码错误';
        loginPwdVallidator.input.value = '';
    }
};