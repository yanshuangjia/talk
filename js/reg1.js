
const loginIdVallidator = new FieldValidator('txtLoginId',async function(val){
    if(!val){
        return '请填写账号';
    }
    const resp = await API.exists(val);
    if(resp.data){
        return '该账号已存在，请重新输入'
    }
});

const nickNameVallidator = new FieldValidator('txtNickname',async function(val){
    if(!val){
        return '请填写昵称';
    }
});

const loginPwdVallidator = new FieldValidator('txtLoginPwd',async function(val){
    if(!val){
        return '请输入密码';
    }
});

const loginPwdConfirmVallidator = new FieldValidator('txtLoginPwdConfirm',async function(val){
    if(!val){
        return '请确认密码';
    };
    if(val !== loginPwdVallidator.input.value){
        return '两次密码不一致';
    }
});

const form = $('.user-form');
form.onsubmit =async function(e){
    //阻止事件的默认行为，让表单不刷新
    e.preventDefault();
    const result = await FieldValidator.validate(
        loginIdVallidator,
        loginPwdConfirmVallidator,
        loginPwdVallidator,
        nickNameVallidator
    );
    if(!result){
        return;
    }
    const resp = await API.reg(
        {
            loginId:loginIdVallidator.input.value,
            loginPwd:loginPwdVallidator.input.value,
            nickname:nickNameVallidator.input.value,
        }
    )
    if(resp.code === 0){
        alert('注册成功跳转到登录页面');
        location.href = './login.html';
    }
};